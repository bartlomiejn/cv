# cv

Usage of classical computer vision techniques or machine/deep learning for:
- Image processing operations
- Feature extraction
- Object recognition 
- Classification

### Building (macOS)

Required packages: `cmake pkg-config wget jpeg libpng libtiff openexr eigen tbb hdf5`

Venv & OpenCV setup: `make setup`

### Running

`make run SRC={script path} PARAMS={optional params}`

